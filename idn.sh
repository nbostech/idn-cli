declare hostname="idn-shell-script-undistempered-mb.au-syd.mybluemix.net"

if [ -d "$HOME/.idn" ]; then
  echo ""
else
   mkdir $HOME/.idn
   echo "Folder Creation success"
fi

if [ "$1" = "signup" ];
then
 if [ "$#" = 9 ]
  then
   echo "Number of arguments satisfied"   
   curl -X GET http://$hostname/$1?args=$2,$3,$4,$5,$6,$7,$8,$9
   else
   echo "please pass all arguments(./idn -fn firstname -ln lastname -email email -password password)"
   exit 1
  fi   
 fi
 
 
if [ "$1" = "login" ];
then
 if [ "$#" = 5 ]
  then
   echo "Number of arguments satisfied"
   touch $HOME/.idn/access_token.json
   curl -X GET http://$hostname/$1?args=$2,$3,$4,$5 > $HOME/.idn/access_token.json
   value=`cat $HOME/.idn/access_token.json`
   touch $HOME/.idn/tenants.json
   curl -X GET http://$hostname/tenants?args='-a',$value > $HOME/.idn/tenants.json
   else
   echo "please pass all arguments (./idn login -u username -p password)"
   exit 1
  fi   
 fi
 
 
  if [ "$1" = "create-tenant" ];
then
 if [ "$#" = 3 ]
  then
   echo "Number of arguments satisfied"
   value=`cat $HOME/.idn/access_token.json`
   echo $value
   curl -X GET http://$hostname/$1?args=$2,$3,'-a',$value
   curl -X GET http://$hostname/tenants?args='-a',$value > $HOME/.idn/tenants.json
   else
   echo "please pass all arguments (./idn create-tenant -t tenant-name)"
   exit 1
  fi   
 fi
 
 
if [ "$1" = "create-client" ];
then
 if [ "$#" = 7 ]
  then
   echo "Number of arguments satisfied"
   value=`cat $HOME/.idn/access_token.json`
   curl -X GET http://$hostname/$1?args=$2,$3,$4,$5,$6,$7
   curl -X GET http://$hostname/tenants?args='-a',$value > $HOME/.idn/tenants.json
   else
   echo "please pass all arguments (./idn crate-client -cn clientname -cs clientsecret -t tenantId)"
   exit 1
  fi   
 fi
 
 
 if [ "$1" = "logout" ];
then
 if [ "$#" = 1 ]
  then
   echo "Number of arguments satisfied"
   rm -rf $HOME/.idn
   echo "Successfully logged out"
   else
   echo "please pass all arguments (./idn logout)"
   exit 1
  fi   
 fi
 